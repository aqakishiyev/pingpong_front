$( document ).ready(function() {
    // Handler for .ready() called.
    // Ranking LIST order
    let rankingOrder = $(".ranking-order-number");
    for (let i = 0; i < rankingOrder.length; i++) {
        rankingOrder[i].innerHTML = (i+1) + ".";
    }

    resize();

});

$( window ).resize(function() {
    resize();
});

function resize() {
    let pOfWeek = $(".player-of-week");
    pOfWeek.css("height", pOfWeek.css("width"));

    let leagueLogo = $(".active-league-block-header div img");
    leagueLogo.css("height", leagueLogo.css("width"));

    $(".active-league-block-header div").css("line-height", leagueLogo.css("height"));

    let leagueWallpaper = $(".league-img-box img");
    leagueWallpaper.css("height", leagueWallpaper.width()*0.3);

    console.log("Widths OK");
}